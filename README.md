INTRODUCTION
------------

This module provides a simple way of integrating the
[Iubenda](https://iubenda.com) service into your Drupal website.

Once configured, the cookie solution configured on Iubenda will be shown on all
pages on your Drupal site, to all visitors to your site. Where possible it'll
use the settings from the Iubenda configuration, rather than overriding through
the Drupal interface.

 * For a full description of the module, visit the project page:
   <https://www.drupal.org/project/iubenda>

 * To submit bug reports and feature suggestions, or to track changes:
   <https://www.drupal.org/project/issues/iubenda>


REQUIREMENTS
------------

In order to use this module, an account on [Iubenda](https://iubenda.com) is
required, and a cookie solution must be set up.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
-------------

 * This module provides a configuration page at **Configuration > System >
   Iubenda**. The page requires two values:

   - **Site ID**: the site ID from Iubenda. This can be found in the URL when
     editing your policy.

   - **Policy ID**: the policy ID from Iubenda. This varies per language.

 * There are some optional configuration items that will change the behavior of
   the banner. For more information, read the [Iubenda docs](https://www.iubenda.com/en/help/1205-how-to-configure-your-cookie-solution-advanced-guide#optional-parameters).

 * The configuration can be set per language enabled on the site by clicking the
   "Translate Iubenda" tab in the configuration. In this case, only the Policy
   ID should change, unless you have different sites set up on Iubenda for each
   of your languages.


KNOWN ISSUES
------------

 * As of 1.0-alpha1, this module only supports the cookie solution. Prior to
   stable launch, the privacy policy will also be added.

 * Some Drupal-provided language IDs do not match up exactly to those required
   by Iubenda, for example Portugese (Brazilian) which is `pt-br` in Drupal but
   should be `pt-BR` in Iubenda. To resolve this, you will need to alter the
   DrupalSettings.


MAINTAINERS
-----------

Current maintainers:
 * Sophie Shanahan-Kluth (Sophie.SK) - <https://drupal.org/user/1540896>

This project has been sponsored by:
 * Focusrite Audio Engineering, Ltd.

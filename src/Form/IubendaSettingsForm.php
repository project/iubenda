<?php

namespace Drupal\iubenda\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * The Iubenda settings form.
 */
class IubendaSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'iubenda.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'iubenda.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('iubenda.settings');

    $form['prefix'] = [
      '#type' => 'markup',
      '#markup' => '<p>' . $this->t('In order to use this module, you need an account set up on <a href="@iubenda_url">Iubenda</a>, and the appropriate cookie policies set up.', ['@iubenda_url' => 'https://www.iubenda.com/']) . '</p>',
    ];

    $form['iubenda'] = [
      '#type' => 'vertical_tabs',
      '#default_tab' => 'edit-iubenda-cookies',
    ];

    $form['iubenda_cookies'] = [
      '#type' => 'details',
      '#title' => $this->t('Cookie solution'),
      '#group' => 'iubenda',
      '#tree' => TRUE,
    ];

    $form['iubenda_cookies']['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable cookie solution'),
      '#default_value' => $config->get('iubenda_cookies.enabled') ?: FALSE,
      '#group' => 'iubenda',
    ];

    $form['iubenda_cookies']['show_on_admin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show on admin routes'),
      '#default_value' => $config->get('iubenda_cookies.show_on_admin') ?: FALSE,
      '#group' => 'iubenda',
    ];

    $form['iubenda_cookies']['settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
      '#group' => 'iubenda',
      '#states' => [
        'visible' => [
          ':input[name="iubenda_cookies[enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['iubenda_cookies']['settings']['site_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site ID'),
      '#description' => $this->t('The site ID from Iubenda. This ID is the same across all languages.'),
      '#default_value' => $config->get('iubenda_cookies.settings.site_id') ?: '',
      '#states' => [
        'required' => [
          ':input[name="iubenda_cookies[enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['iubenda_cookies']['settings']['policy_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Policy ID'),
      '#description' => $this->t('The cookie policy ID from Iubenda. This should be language-specific.'),
      '#default_value' => $config->get('iubenda_cookies.settings.policy_id') ?: '',
      '#states' => [
        'required' => [
          ':input[name="iubenda_cookies[enabled]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['iubenda_cookies']['settings']['show_gdpr'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show GDPR'),
      '#default_value' => $config->get('iubenda_cookies.settings.show_gdpr') ?: FALSE,
    ];

    $form['iubenda_cookies']['settings']['show_ccpa'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show CCPA'),
      '#default_value' => $config->get('iubenda_cookies.settings.show_ccpa') ?: FALSE,
    ];

    $form['iubenda_cookies']['settings']['country_detection'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable country detection'),
      '#default_value' => $config->get('iubenda_cookies.settings.country_detection') ?: FALSE,
    ];

    $form['iubenda_cookies']['settings']['consent_on_browsing'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable consent on continued browsing'),
      '#default_value' => $config->get('iubenda_cookies.settings.consent_on_browsing') ?: FALSE,
      '#description' => $this->t('Enabling this will cause the policy to be accepted on any interaction with the rest of the page.')
    ];

    $form['iubenda_cookies']['settings']['per_purpose_consent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable per-purpose consent controls'),
      '#default_value' => $config->get('iubenda_cookies.settings.per_purpose_consent') ?: FALSE,
      '#description' => $this->t('Enabling this will allow users to customise their consent by purpose.')
    ];

    $form['iubenda_cookies']['settings']['buttons'] = [
      '#type' => 'details',
      '#title' => $this->t('Button configuration'),
    ];

    $form['iubenda_cookies']['settings']['buttons']['accept_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the "accept" button'),
      '#default_value' => $config->get('iubenda_cookies.settings.buttons.accept_button') ?: FALSE,
    ];

    $form['iubenda_cookies']['settings']['buttons']['accept_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Accept" button text'),
      '#default_value' => $config->get('iubenda_cookies.settings.buttons.accept_button_text') ?: $this->t('Accept'),
      '#states' => [
        'required' => [
          ':input[name="iubenda_cookies[settings][buttons][accept_button]"]' => ['checked' => TRUE],
        ],
        'visible' => [
          ':input[name="iubenda_cookies[settings][buttons][accept_button]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['iubenda_cookies']['settings']['buttons']['reject_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the "reject" button'),
      '#default_value' => $config->get('iubenda_cookies.settings.buttons.reject_button') ?: FALSE,
    ];

    $form['iubenda_cookies']['settings']['buttons']['customize_button'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show the "learn more and customize" button'),
      '#default_value' => $config->get('iubenda_cookies.settings.buttons.customize_button') ?: FALSE,
    ];

    $form['iubenda_cookies']['settings']['position'] = [
      '#type' => 'select',
      '#title' => $this->t('Banner position'),
      '#description' => $this->t('Note that the position chosen may interfere with other UI elements on the web page.'),
      '#options' => [
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
        'float-top-left' => $this->t('Float top left'),
        'float-top-right' => $this->t('Float top right'),
        'float-bottom-left' => $this->t('Float bottom left'),
        'float-bottom-right' => $this->t('Float bottom right'),
        'float-top-center' => $this->t('Float top center'),
        'float-bottom-center' => $this->t('Float bottom center'),
        'float-center' => $this->t('Float center'),
      ],
      '#default_value' => $config->get('iubenda_cookies.settings.position') ?: 'float-top-center',
    ];

    $form['iubenda_cookies']['settings']['apply_styles'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Apply styles'),
      '#description' => $this->t('On: Iubenda styles are applied; Off: Custom css from theme may be applied'),
      '#default_value' => $config->get('iubenda_cookies.settings.apply_styles') ?: TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->config('iubenda.settings')
      ->set('iubenda_cookies.enabled', $values['iubenda_cookies']['enabled'])
      ->set('iubenda_cookies.show_on_admin', $values['iubenda_cookies']['show_on_admin'])
      ->set('iubenda_cookies.settings', $values['iubenda_cookies']['settings'])
      ->save();

    return parent::submitForm($form, $form_state);
  }

}
